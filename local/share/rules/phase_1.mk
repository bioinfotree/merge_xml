### rules.mk --- 
## 
## Filename: rules.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Oct 24 15:34:17 2012 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

context prj/annotation

SINGLE ?=
PATTERN ?= 


# if it is a single file with multiple xml documents inside
define onefile
	TMP=$$(mktemp tmpXXXX); \
	sed -i '/^$$/d' $(ORIGINAL); \
	head -q -n 20 < $(ORIGINAL) >> $$TMP; \
	awk ' \
	/<Iteration>/,/<\/Iteration>/ { s=s ORS $$0 } \
	/<\/Iteration>/ \
	{ printf "%s",s; s="" } \
	' $(ORIGINAL) >> $$TMP; \
	tail -q -n 2 < $(ORIGINAL) >>$$TMP; \
	sed -i '/^$$/d' $$TMP; \
	if [ -s $$TMP ]; then \
	gzip -c $$TMP >$1; \
	rm $$TMP; \
	fi

endef


FILE_LIST = $(shell ls -1 $(PATTERN) | tr \\n " ")

# if they are multiple files with one document each one
define multifile
	TMP=$$(mktemp tmpXXXX); \
	mergexml -o $$TMP -i $(FILE_LIST); \
	if [ -s $$TMP ]; then \
	gzip -c $$TMP >$1; \
	rm $$TMP; \
	fi
endef


$(OUTFILE).xml.gz:
ifdef PATTERN
	$(call multifile,$@)
else
	$(call onefile,$@)
endif



test.tab: joined.xml
	blast_fields -r query <$<


ALL += $(OUTFILE).xml.gz

INTERMEDIATE += 

CLEAN += 

######################################################################
### rules.mk ends here
